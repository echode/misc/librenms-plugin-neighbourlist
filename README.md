# librenms-plugin-NeighbourList

A LibreNMS plugin to show a searchable list of all neighbours found by lldp/cdp.

_Made for Aurskog-Høland kommune_


# Install

Rename and copy this directory to librenms/html/plugins/NeighbourList.

In LibreNMS go to Overview->Plugins->Plugin Admin.

Click Enable on NeighbourList.


# Usage

Go to Overview->Plugins->Neighbour List.