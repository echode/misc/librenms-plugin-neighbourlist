<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default panel-condensed">
        <div class="panel-heading">
          <strong>Neighbour List</strong>
        </div>
        <table id="link-search" class="table table-hover table-condensed table-striped">
          <thead>
            <tr>
              <th data-column-id="device_name" data-formatter="link">Device Name</th>
              <th data-column-id="device_port" data-formatter="link">Device Port</th>
              <th data-column-id="connection_name" data-formatter="link">Remote Name</th>
              <th data-column-id="connection_port" data-formatter="link" data-searchable="false">Remote Port</th>
              <th data-column-id="connection_desc">Remote Description</th>
              <th data-column-id="discovery_proto" data-searchable="false">Discovery Protocol</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $("#link-search").bootgrid({
    rowCount: [50,100,250,-1],
    columnSelection: true,
    caseSensitive: false,
    multiSort: true,
    ajax: true,
    url: "/plugins/NeighbourList/ajax.php",
    formatters: {
      "link": function(column,row) {
        if (typeof row[column.id] === 'object' && row[column.id]['text'] != undefined) {
          let output = row[column.id]['text']
          if (row[column.id]['url'] != undefined) {
            let cssclass = ''
            if (column.id == "device_name") { cssclass = 'list-device' }
            output = '<a href="' + row[column.id]['url'] + '" class="' + cssclass + '">' + row[column.id]['text'] + '</a>'
          }
          if (row[column.id]['status'] == "down") { 
            output += '<i class="fa fa-flag fa-lg" style="color:red" aria-hidden="true" title="Remote Port is down"></i>'
          }
          return output
        }
        return row[column.id]
      }
    }
  });
</script>